@extends('layout.master')

@section('title-1', 'Edit Profil')
@section('title-2', Auth::user()->name)

@section('content')
  <div class="col-sm-8 mx-auto">
    <div class="foto text-center mb-4">
        <img src="/img/profil/{{ $profil->foto }}" alt="{{ $profil->user->name }}" class="img-circle" style="max-width: 400px; height: auto;">
    </div>
    <p class="login-box-msg">Anda dapat melakukan perubahan data diri Anda di halaman ini.</p>
    <form action="profil/{{ $profil->id }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <div class="input-group mt-4 mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="uploadFoto">Upload</span>
              </div>
              <div class="custom-file">
                <input type="file" class="custom-file-input" name="foto" id="foto" aria-describedby="uploadFoto">
                <label class="custom-file-label" for="foto">{{ $profil->foto }}</label>
              </div>
            </div>
            @error('foto')
                <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mb-3">
            <label for="name">Nama</label>
            <input type="text" name="name" id="name" class="form-control" placeholder="Nama" value="{{ $profil->user->name }}">
        </div>
        @error('nama')
        <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
        @enderror

        <div class="form-group mb-3">
            <label for="email">Email</label>
            <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="{{ $profil->user->email }}" disabled>
        </div>
        
        <div class="form-group mb-3">
            <label for="umur">Umur</label>
            <input type="text" name="umur" id="umur" class="form-control" placeholder="Umur" pattern="[1-9][0-9]*" value="{{ $profil->umur }}">
        </div>
        @error('umur')
        <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
        @enderror

        <div class="form-group mb-3">
            <label for="bio">Biodata</label>
            <textarea name="bio" id="bio" class="form-control" placeholder="Biodata">{{ $profil->bio }}</textarea>
        </div>
        @error('bio')
        <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
        @enderror

        <div class="form-group mb-3">
            <label for="alamat">Alamat Lengkap</label>
            <textarea name="alamat" id="alamat" class="form-control" placeholder="Alamat Lengkap Anda">{{ $profil->alamat }}</textarea>
        </div>
        @error('alamat')
        <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
        @enderror
        
        <div class="row">
            <!-- /.col -->
            <div class="col-12">
                <button type="submit" class="btn btn-primary d-block mx-auto mb-2">Simpan</button>
            </div>
            <!-- /.col -->
        </div>
    </form>
  </div>
@endsection

@push('script')
    <script src="https://cdn.tiny.cloud/1/uvl9nj9mcb7i6erooi8qxrl7ltzsnaosgyje9y74cmdikwl2/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            toolbar_mode: 'floating',
        });
    </script>
    <script>
        $(document).ready(function () {
            bsCustomFileInput.init()
        })
    </script>
@endpush