@extends('layout.master')

@section('title-1', 'Selamat Datang di Review Buku')
@section('title-2', 'Home')

@push('css')
<link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="{{asset('admin/plugins/char.js/Chart.min.css')}}">
@endpush

@section('content')
<div class="row justify-content-center">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-title text-center p-2">
                <h2>Buku Terbaru</h2>
            </div>
            <div class="card-body">
                <div class="owl-carousel owl-theme">
                @forelse ($bukux as $book)
                <div class="item">
                    <div class="card">
                        <div class="card-title"></div>
                        <div class="card-body">
                            <a href="/buku/{{ $book->id }}">
                                <img src="img/cover/{{ $book->coverimg }}" alt="{{ $book->judul }}">
                            </a>
                        </div>
                    </div>
                </div>
                @empty
                    <p>Data masih kosong.</p>
                @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-title text-center p-2">
                <h2>Jumlah Ulasan Terbanyak</h2>
            </div>
            <div class="card-body">
                <canvas id="myChart" style="height: auto; max-width:1200px;" class="mx-auto"></canvas>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-title text-center p-2">
        <h2>Semua Buku</h2>
    </div>
    <div class="card-body">
        <div class="row">
            @forelse ($bukus as $buku)
            <div class="col-sm-4">
                <div class="card">
                    <img src="{{asset('img/cover/'.$buku->coverimg)}}" alt="{{ $buku->judul }}" class="card-img-top img-responsive">
                    <div class="card-body">
                        <h4 class="card-title text-bold text-primary mb-1">{{ $buku->judul }}</h4>
                        @if ($buku->tanggapan->avg('rating') > 0)
                        <div class="rating card-text d-flex justify-content-between">
                            <ul class="list-unstyled list-inline mb-0">
                                @for ($i = 0; $i < 5; $i++)
                                    @if (floor($buku->tanggapan->avg('rating'))-$i >=1)
                                        <li class="list-inline-item"><i class="fa fa-star text-warning"></i></li>
                                    @else
                                        @if ($buku->tanggapan->avg('rating')-$i > 0)
                                            <li class="list-inline-item"><i class="fa fa-star-half-alt text-warning"></i></li>
                                        @else
                                        <li class="list-inline-item"><i class="far fa-star text-secondary"></i></li>
                                        @endif
                                    @endif
                                @endfor
                              </ul>
                              <p class="badge badge-success">{{ number_format($buku->tanggapan->avg('rating'), 1, '.') }}</p>
                        </div>
                        @endif
                        <p class="card-text">{!! \Illuminate\Support\Str::limit($buku->sinopsis, 100, '...') !!}</p>
                        <a href="/buku/{{ $buku->id }}" class="text-bold text-dark"> READ MORE</a>
                        <div class="button-container text-right">
                            @auth
                            <a href="/buku/{{ $buku->id }}/edit" class="btn btn-warning btn-xs text-white" style="min-width: 24px;" data-toggle="tooltip" data-placement="top" title="Edit {{ $buku->judul }}">
                                <i class="fa fa-edit"></i>
                            </a>
                            <form action="/buku/{{ $buku->id }}" method="POST" class="d-inline-block">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus {{ $buku->judul }}" style="min-width: 24px;" onclick="return confirm('Anda yakin menghapus {{ $buku->judul }}  ?')">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </form>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
            @empty
                <h4>DATA MASIH KOSONG</h4>
            @endforelse
        </div>
    </div>
</div>
@endsection

@push('script')
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('admin/plugins/chart.js/Chart.min.js')}}"></script>
<script>
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    dots: true,
    autoplay: true,
    smartSpeed: 1000,
    dotSpeed: 100,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
});
</script>
<script>
    const ctx = document.getElementById('myChart');
    const myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: {!! $labels !!},
            datasets: [{
                label: 'Jumlah ulasan',
                data: {!! $datas !!},
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
    </script>
@endpush