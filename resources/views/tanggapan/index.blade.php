@extends('layout.master')

@section('title-1', 'Halaman Ulasan')
@section('title-2', 'Daftar Ulasan')

@push('css')
    <link rel="stylesheet" href="{{asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}"/>
@endpush

@section('content')
<div class="d-flex justify-content-end">
  <a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-4"><i class="fa fa-reply"></i> Kembali</a>
</div>
@if (session()->has('error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Failed!</h4>
        <p>{{ session()->get('error') }}</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@elseif (session()->has('success')) 
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Success!</h4>
        <p>{{ session()->get('success') }}</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<table id="buku" class="table table-bordered table-striped">
  <thead>
    <tr>
      <th class="text-center">#</th>
      <th class="text-center">Nama</th>
      <th class="text-center">Rating</th>
      <th class="text-center">Ulasan</th>
      <th class="text-center">Buku</th>
      <th class="text-center">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($tanggapans as $key => $tanggapan)
    <tr>
      <td class="text-center">{{ $key+1 }}</td>
      <td>{{ $tanggapan->user->name }}</td>
      <td>{{ $tanggapan->rating }}</td>
      <td>{!! $tanggapan->isi !!}</td>
      <td>
          <ul class="list-unstyled">
              <li>{{ $tanggapan->buku->judul }}</li>
              <li>{{ $tanggapan->buku->penulis->nama }}</li>
              <li>{{ $tanggapan->buku->tahun_terbit }}</li>
          </ul>
      </td>
      <td class="text-center text-nowrap">
          <a href="/tanggapan/{{ $tanggapan->id }}/edit" class="btn btn-warning btn-sm text-white" data-toggle="tooltip" data-placement="top" title="Edit Ulasan {{ $tanggapan->user->name }}" style="min-width: 34px;">
              <i class="fa fa-edit"></i>
          </a>
          <form action="/tanggapan/{{ $tanggapan->id }}" method="POST" class="d-inline-block">
              @method('delete')
              @csrf
              <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus Tanggapan {{ $tanggapan->user->name }}" style="min-width: 34px;" onclick="return confirm('Anda yakin menghapus {{ $tanggapan->user->name }}  ?')">
                  <i class="fa fa-trash"></i>
              </button>
          </form>
      </td>
    </tr>
    @empty
    <tr>
      <td colspan="6" class="text-center">DATA MASIH KOSONG</td>
    </tr>
    @endforelse
    </tbody>
  <tfoot>
    <tr>
        <th class="text-center">#</th>
        <th class="text-center">Nama</th>
        <th class="text-center">Rating</th>
        <th class="text-center">Ulasan</th>
        <th class="text-center">Buku</th>
        <th class="text-center">Action</th>
    </tr>
  </tfoot>
  </table>
@endsection

@push('script')
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#buku").DataTable();
    });
    </script>
@endpush