@extends('layout.master')

@section('title-1', 'Halaman Ulasan')
@section('title-2', 'Edit Ulasan')

@push('css')
    <link rel="stylesheet" href="{{asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}"/>
    <style>
      div.stars {
        width: 270px;
        display: inline-block;
      }
  
      input.star { display: none; }
  
      label.star {
        float: right;
        padding: 10px;
        font-size: 36px;
        color: #444;
        transition: all .2s;
      }
  
      input.star:checked ~ label.star:before {
        content: '\f005';
        color: #FD4;
        transition: all .25s;
      }
  
      input.star-5:checked ~ label.star:before {
        color: #FE7;
        text-shadow: 0 0 20px #952;
      }
  
      input.star-1:checked ~ label.star:before { color: #F62; }
  
      label.star:hover { transform: rotate(-15deg) scale(1.3); }
  
      label.star:before {
        content: '\f006';
        font-family: FontAwesome;
      }
    </style>
@endpush

@section('content')
<div class="d-flex justify-content-end">
  <a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-4"><i class="fa fa-reply"></i> Kembali</a>
</div>
@if (session()->has('error'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Failed!</h4>
        <p>{{ session()->get('error') }}</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@elseif (session()->has('success')) 
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <h4 class="alert-heading">Success!</h4>
        <p>{{ session()->get('success') }}</p>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<div class="col-sm-8 mx-auto">
  <div class="d-flex">
      <a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
  </div>
  <form action="/tanggapan/{{ $tanggapan->id }}" method="POST">
    @method('put')
    @csrf
    <input type="hidden" name="buku_id" id="buku_id" value="{{ $tanggapan->buku->id }}">
    <div class="card">
      <div class="card-title"></div>
      <div class="card-body">
        <h4 class="text-center">Ubah Ulasan</h4>
        <div class="form-group text-center mb-0">
          <div class="stars">
            <input class="star star-5" id="star-5" type="radio" name="rating" value="5" <?= ($tanggapan->rating == 5) ? "checked" : ""; ?> >
            <label class="star star-5" for="star-5"></label>
            <input class="star star-4" id="star-4" type="radio" name="rating" value="4" <?= ($tanggapan->rating == 4) ? "checked" : ""; ?>>
            <label class="star star-4" for="star-4"></label>
            <input class="star star-3" id="star-3" type="radio" name="rating" value="3" <?= ($tanggapan->rating == 3) ? "checked" : ""; ?>>
            <label class="star star-3" for="star-3"></label>
            <input class="star star-2" id="star-2" type="radio" name="rating" value="2" <?= ($tanggapan->rating == 2) ? "checked" : ""; ?>>
            <label class="star star-2" for="star-2"></label>
            <input class="star star-1" id="star-1" type="radio" name="rating" value="1" <?= ($tanggapan->rating == 1) ? "checked" : ""; ?>>
            <label class="star star-1" for="star-1"></label>
          </div>
          @error('rating')
          <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="isi">Ulasan</label>
          <textarea name="isi" id="isi" class="form-control">{{$tanggapan->isi}}</textarea>
          @error('isi')
          <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group d-flex justify-content-end">
          <input type="submit" value="Submit" class="btn btn-primary">
        </div>
      </div>
    </div>
  </form>
</div>
@endsection

@push('script')
    <script src="https://cdn.tiny.cloud/1/uvl9nj9mcb7i6erooi8qxrl7ltzsnaosgyje9y74cmdikwl2/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            toolbar_mode: 'floating',
        });
    </script>
@endpush