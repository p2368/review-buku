@extends('layout.master')

@section('title-1', 'Halaman Buku')
@section('title-2', 'Daftar Buku')

@push('css')
    <link rel="stylesheet" href="{{asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}"/>
    <style>
      .bukuWrapper {
        max-width: 100%;
        margin: 0 auto;
      }
      #buku th, td {
        white-space: nowrap;
      }

      table.dataTable thead .sorting:after,
      table.dataTable thead .sorting:before,
      table.dataTable thead .sorting_asc:after,
      table.dataTable thead .sorting_asc:before,
      table.dataTable thead .sorting_asc_disabled:after,
      table.dataTable thead .sorting_asc_disabled:before,
      table.dataTable thead .sorting_desc:after,
      table.dataTable thead .sorting_desc:before,
      table.dataTable thead .sorting_desc_disabled:after,
      table.dataTable thead .sorting_desc_disabled:before {
        bottom: .5em;
      }
    </style>
@endpush

@section('content')
<div class="d-flex justify-content-between">
  <a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-4"><i class="fa fa-reply"></i> Kembali</a>
  <a href="/buku/create"class="btn btn-primary mb-4">Tambah Buku</a>
</div>
<div class="bukuWrapper">
  <table id="buku" class="table table-bordered table-striped table-sm">
    <thead>
      <tr>
        <th class="text-center" width="2%">#</th>
        <th class="text-center" width="8%">Cover</th>
        <th class="text-center" width="12%">Judul</th>
        <th class="text-center text-wrap" width="10%">Penulis</th>
        <th class="text-center" width="10%">Penerbit</th>
        <th class="text-center text-wrap" width="5%">Tahun Terbit</th>
        <th class="text-center text-wrap" width="5%">Jumlah Halaman</th>
        <th class="text-center" width="30%">Sinopsis</th>
        <th class="text-center text-wrap" width="10%">Kategori</th>
        <th class="text-center" width="8%">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($bukus as $key => $buku)
      <tr>
        <td class="text-center">{{ $key+1 }}</td>
        <td class="text-center"><img src="{{asset('img/cover/' . $buku->coverimg )}}" alt="{{ $buku->judul }}" style="max-width: 100px;" class="img-responsive"></td>
        <td class="text-wrap">{{ $buku->judul }}</td>
        <td class="text-wrap">{{ $buku->penulis->nama }}</td>
        <td class="text-wrap">{{ $buku->penerbit }}</td>
        <td class="text-wrap text-center">{{ $buku->tahun_terbit }}</td>
        <td class="text-wrap text-center">{{ $buku->jumlah_halaman }}</td>
        <td class="text-wrap">{!!\Illuminate\Support\Str::limit($buku->sinopsis, 100, '...')!!}</td>
        <td class="text-wrap">{{ $buku->kategori->nama }}</td>
        <td class="text-center text-nowrap">
            <a href="/buku/{{ $buku->id }}" class="btn btn-info btn-xs btn" data-toggle="tooltip" data-placement="top" title="Detail {{ $buku->judul }}" style="min-width: 25px;">
                <i class="fa fa-info"></i>
            </a>
            <a href="/buku/{{ $buku->id }}/edit" class="btn btn-warning btn-xs text-white" data-toggle="tooltip" data-placement="top" title="Edit {{ $buku->judul }}" style="min-width: 25px;">
                <i class="fa fa-edit"></i>
            </a>
            <form action="/buku/{{ $buku->id }}" method="POST" class="d-inline-block">
                @method('delete')
                @csrf
                <button type="submit" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Hapus {{ $buku->judul }}" style="min-width: 25px;" onclick="return confirm('Anda yakin menghapus {{ $buku->judul }}  ?')">
                    <i class="fa fa-trash"></i>
                </button>
            </form>
        </td>
      </tr>
      @empty
      <tr>
        <td colspan="10" class="text-center">DATA MASIH KOSONG</td>
      </tr>
      @endforelse
      </tbody>
    <tfoot>
      <tr>
        <th class="text-center">#</th>
        <th class="text-center">Cover</th>
        <th class="text-center">Judul</th>
        <th class="text-center text-wrap">Penulis</th>
        <th class="text-center">Penerbit</th>
        <th class="text-center text-wrap">Tahun Terbit</th>
        <th class="text-center text-wrap">Jumlah Halaman</th>
        <th class="text-center">Sinopsis</th>
        <th class="text-center text-wrap">Kategori</th>
        <th class="text-center">Action</th>
      </tr>
    </tfoot>
    </table>
</div>
@endsection

@push('script')
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    // $(function () {
    //     $("#buku").DataTable();
    // });
    $(document).ready(function () {
      $('#buku').DataTable({
        "scrollX": true
      });
      $('.dataTables_length').addClass('bs-select');
    });
    </script>
@endpush