@extends('layout.master')

@section('title-1', 'Halaman Buku')
@section('title-2', 'Detail ' . $buku->judul)

@push('css')
<style>
    div.stars {
      width: 270px;
      display: inline-block;
    }

    input.star { display: none; }

    label.star {
      float: right;
      padding: 10px;
      font-size: 36px;
      color: #444;
      transition: all .2s;
    }

    input.star:checked ~ label.star:before {
      content: '\f005';
      color: #FD4;
      transition: all .25s;
    }

    input.star-5:checked ~ label.star:before {
      color: #FE7;
      text-shadow: 0 0 20px #952;
    }

    input.star-1:checked ~ label.star:before { color: #F62; }

    label.star:hover { transform: rotate(-15deg) scale(1.3); }

    label.star:before {
      content: '\f006';
      font-family: FontAwesome;
    }
  </style>
@endpush

@section('content')
<div class="row d-block">
  <ul class="list-unstyled d-none d-sm-block">
    <li class="media">
      <img src="{{ asset('img/cover/'.$buku->coverimg) }}" alt="{{ $buku->judul }}" class="mr-3 img-responsive" style="max-width: 250px">
      <div class="media-body">
        <h2 class="mt-0 mb-1 text-bold">{{ $buku->judul }} ({{ $buku->tahun_terbit }})</h2>
        <h6>
          <a href="/kategori/{{ $buku->kategori->id }}">
            <span class="badge badge-warning">{{ $buku->kategori->nama }}</span>
          </a>
        </h6>
        <blockquote class="blockquote ml-0">
          <p class="mb-0"><?= $buku->sinopsis; ?></p>
          <footer class="blockquote-footer">Sinopsis oleh <cite title="Source Title">Review Buku</cite></footer>
        </blockquote>
        <dl class="row">
          <dt class="col-2">Penulis</dt>
          <dd class="col-10">
            <a href="/penulis/{{ $buku->penulis_id }}">{{ $buku->penulis->nama }}</a>
          </dd>
        </dl>
      </div>
    </li>
  </ul>
  <div class="d-block d-sm-none">
    <img src="{{ asset('img/cover/'.$buku->coverimg) }}" alt="{{ $buku->judul }}" class="mr-3 img-responsive w-100">
    <h2 class="mt-0 mb-1 text-bold">{{ $buku->judul }} ({{ $buku->tahun_terbit }})</h2>
    <h6>
      <a href="/kategori/{{ $buku->kategori->id }}">
        <span class="badge badge-warning">{{ $buku->kategori->nama }}</span>
      </a>
    </h6>
    <dl class="row">
        <dt class="col-2">Penulis</dt>
        <dd class="col-10">
            <a href="/penulis/{{ $buku->penulis_id }}">{{ $buku->penulis->nama }}</a>
        </dd>
    </dl>
    <blockquote class="blockquote ml-0 mr-0 pr-0">
        <p class="mb-0"><?= $buku->sinopsis; ?></p>
        <footer class="blockquote-footer">Sinopsis oleh <cite title="Source Title">Review Buku</cite></footer>
    </blockquote>
  </div>
  <div class="back-button">
    <a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-3 text-right"><i class="fa fa-reply"></i> Kembali</a>
  </div>
  <div class="ulasan" id="ulasan">
    <h3 class="text-center mb-4">Ulasan</h3>
    <dl class="row justify-content-center">
      @forelse ($buku->tanggapan as $tanggapan)
      <dt class="col-2"><small><strong>{{$tanggapan->user->name}}</strong></small></dt>
      <dd class="col-9">
        <div class="card bg-light p-2">
          <ul class="list-unstyled list-inline mb-0">
            @for ($i = 0; $i < 5; $i++)
                @if (floor($tanggapan->rating)-$i >=1)
                    <li class="list-inline-item"><i class="fa fa-star text-warning"></i></li>
                @else
                    @if ($tanggapan->rating-$i > 0)
                        <li class="list-inline-item"><i class="fa fa-star-half-alt text-warning"></i></li>
                    @else
                        <li class="list-inline-item"><i class="far fa-star text-secondary"></i></li>
                    @endif
                @endif
            @endfor
          </ul>
          <hr class="mt-1 mb-1">
          <p>{!!$tanggapan->isi!!}</p>
          @if ($tanggapan->user_id == Auth::id())
          <div class="d-flex justify-content-end align-items-center button-review">
            <a href="/tanggapan/{{$tanggapan->id}}/edit" class="btn btn-warning btn-xs mr-1" style="width: 26px; height: 26px"><i class="fa fa-edit"></i></a>
            <form action="/tanggapan/{{ $tanggapan->id }}" method="POST" class="d-inline-block">
              @method('delete')
              @csrf
              <button type="submit" class="btn btn-danger btn-xs" style="width: 26px; height: 26px" onclick="return confirm('Anda yakin menghapus ulasan anda?')">
                  <i class="fa fa-trash"></i>
              </button>
            </form>
          </div>
          @endif
        </div>
      </dd>
      @empty
      <div class="col-12">
        <div class="card bg-light w-100 p-2">
          <p class="text-center mb-0">Belum ada ulasan.</p>
        </div>
      </div>
      @endforelse
    </dl>
    @auth
    @if ($buku->find($buku->id)->tanggapan()->where('user_id', Auth::id())->first())
        
    @else
    <form action="/tanggapan" method="POST">
      @csrf
      <div class="card">
        <div class="card-title"></div>
        <div class="card-body">
          <h4 class="text-center">Beri Ulasan</h4>
          <input type="hidden" name="buku_id" value="{{ $buku->id }}">
          <div class="form-group text-center mb-0">
            <div class="stars">
              <input class="star star-5" id="star-5" type="radio" name="rating" value="5">
              <label class="star star-5" for="star-5"></label>
              <input class="star star-4" id="star-4" type="radio" name="rating" value="4">
              <label class="star star-4" for="star-4"></label>
              <input class="star star-3" id="star-3" type="radio" name="rating" value="3">
              <label class="star star-3" for="star-3"></label>
              <input class="star star-2" id="star-2" type="radio" name="rating" value="2">
              <label class="star star-2" for="star-2"></label>
              <input class="star star-1" id="star-1" type="radio" name="rating" value="1">
              <label class="star star-1" for="star-1"></label>
            </div>
            @error('rating')
            <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label for="isi">Ulasan</label>
            <textarea name="isi" id="isi" class="form-control"></textarea>
            @error('isi')
            <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group d-flex justify-content-end">
            <input type="submit" value="Submit" class="btn btn-primary">
          </div>
        </div>
      </div>
    </form>
    @endif
    @endauth
  </div>
</div>
@endsection

@push('script')
    <script src="https://cdn.tiny.cloud/1/uvl9nj9mcb7i6erooi8qxrl7ltzsnaosgyje9y74cmdikwl2/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            toolbar_mode: 'floating',
        });
    </script>
@endpush