@extends('layout.master')

@section('title-1', 'Halaman Buku')
@section('title-2', 'Edit ' . $buku->judul)

@push('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<style>
    span.selection span.select2-selection {
        padding-top: 2px;
        padding-bottom: 2px;
        min-height: 38px
    }

    span.selection span.select2-selection__rendered {
        line-height: 38px !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 33px;
    }
</style>
@endpush

@section('content')
<div class="col-sm-8 mx-auto">
    <div class="d-flex">
        <a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
    </div>
    <form action="/buku/{{ $buku->id }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
          <label for="judul">Judul Buku</label>
          <input type="text" class="form-control" name="judul" id="judul" value="{{ $buku->judul }}">
          @error('judul')
              <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
          @enderror
        </div>
        <div class="row">
            <div class="form-group col-sm-6">
                <label for="penulis_id">Penulis</label>
                <select name="penulis_id" id="penulis_id" class="custom-select">
                  @forelse ($penulises as $penulis)
                      <option value="{{ $penulis->id }}" @if ($penulis->id == $buku->penulis_id)
                          selected
                      @endif>{{ $penulis->nama }}</option>
                  @empty
                      <option disabled>BELUM ADA DATA</option>
                  @endforelse
                </select>
                @error('penulis_id')
                    <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group col-sm-6">
                <label for="penulis_baru">Penulis</label>
                <input type="text" name="penulis_baru" id="penulis_baru" class="form-control" placeholder="Input penulis baru jika tidak ada dalam pilihan.">
                @error('penulis_baru')
                    <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for="penerbit">Penerbit</label>
            <input type="text" class="form-control" name="penerbit" id="penerbit" value="{{ $buku->penerbit }}">
            @error('penerbit')
                <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="tahun_terbit">Tahun Terbit</label>
            <input type="text" pattern="[1-2][0-9][0-9][0-9]" class="form-control" name="tahun_terbit" id="tahun_terbit" value="{{ $buku->tahun_terbit }}">
            @error('tahun_terbit')
                <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="jumlah_halaman">Jumlah Halaman</label>
            <input type="text" pattern="[1-9][0-9]*" class="form-control" name="jumlah_halaman" id="jumlah_halaman" value="{{ $buku->jumlah_halaman }}">
            @error('jumlah_halaman')
                <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="sinopsis">Sinopsis</label>
            <textarea name="sinopsis" id="sinopsis" class="form-control">{{ $buku->sinopsis }}</textarea>
            @error('sinopsis')
                <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
            @enderror
        </div>
        <div class="row">
            <div class="form-group col-sm-6">
                <label for="kategori_id">Kategori</label>
                <select name="kategori_id" id="kategori_id" class="custom-select">
                  @forelse ($kategoris as $kategori)
                      <option value="{{ $kategori->id }}" @if ($kategori->id == $buku->kategori_id)
                          selected
                      @endif>{{ $kategori->nama }}</option>
                  @empty
                      <option disabled>BELUM ADA DATA</option>
                  @endforelse
                </select>
                @error('kategori_id')
                    <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group col-sm-6">
                <label for="kategori_baru">Kategori Baru</label>
                <input type="text" name="kategori_baru" id="kategori_baru" class="form-control" placeholder="Input kategori baru jika tidak ada dalam pilihan.">
                @error('kategori_baru')
                    <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <div class="input-group mt-4 mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="uploadCover">Upload</span>
              </div>
              <div class="custom-file">
                <input type="file" class="custom-file-input" name="coverimg" id="coverimg" aria-describedby="uploadCover">
                <label class="custom-file-label" for="coverimg">{{ $buku->coverimg }}</label>
              </div>
            </div>
            @error('coverimg')
                <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
            @enderror
        </div>
        <div class="d-flex justify-content-between">
            <button type="submit" class="btn btn-primary">Submit</button>
            <input type="reset" value="Reset" class="btn btn-secondary">
        </div>
    </form>
</div>
@endsection

@push('script')
    <script src="https://cdn.tiny.cloud/1/uvl9nj9mcb7i6erooi8qxrl7ltzsnaosgyje9y74cmdikwl2/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            bsCustomFileInput.init()
        })
    </script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            toolbar_mode: 'floating',
        });
    </script>
    <script>
        $(document).ready(function() {
            $('.custom-select').select2();
        });
    </script>
@endpush