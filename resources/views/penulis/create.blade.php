@extends('layout.master')

@section('title-1', 'Halaman Penulis')
@section('title-2', 'Tambah Penulis')

@section('content')
<div class="col-sm-8 mx-auto">
    <div class="d-flex">
        <a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
    </div>
    <form action="/penulis" method="POST">
        @csrf
        <div class="form-group">
          <label for="nama">Nama Penulis</label>
          <input type="text" class="form-control" name="nama" id="nama">
          @error('nama')
              <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
            <label for="bio">Biodata</label>
            <textarea name="bio" id="bio" class="form-control"></textarea>
            @error('bio')
                <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
            @enderror
          </div>
        <div class="d-flex justify-content-between">
            <button type="submit" class="btn btn-primary">Submit</button>
            <input type="reset" value="Reset" class="btn btn-secondary">
        </div>
    </form>
</div>
@endsection

@push('script')
    <script src="https://cdn.tiny.cloud/1/uvl9nj9mcb7i6erooi8qxrl7ltzsnaosgyje9y74cmdikwl2/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            toolbar_mode: 'floating',
        });
    </script>
@endpush