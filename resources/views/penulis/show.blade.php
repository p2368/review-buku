@extends('layout.master')

@section('title-1', 'Halaman Penulis')
@section('title-2', 'Detail ' . $penulis->nama)

@section('content')
<dl class="row">
  <dt class="col-3">Nama Penulis</dt>
  <dd class="col-9">{{ $penulis->nama }}</dd>
  <dt class="col-3">Biodata Penulis</dt>
  <dd class="col-9">{!! $penulis->bio !!}</dd>
  <dt class="col-3">Daftar Buku</dt>
  <dd class="col-9">
    <ul style="padding-left: 20px;">
      @forelse ($penulis->buku as $buku)
          <li><a href="/buku/{{$buku->id}}">{{ $buku->judul }} ({{ $buku->tahun_terbit }})</a></li>
      @empty
          <h4>DATA MASIH KOSONG</h4>
      @endforelse
    </ul>
  </dd>
</dl>
<div class="back-button">
  <a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
</div>
@endsection