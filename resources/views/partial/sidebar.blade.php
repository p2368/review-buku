<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="{{asset('admin/dist/img/buku.jpg')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Review Buku</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex align-items-center">
        @guest
        <div class="image">
          <img src="{{asset('img/profil/avatar.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        @endguest
        @auth
        <div class="image">
          <img src="/img/profil/{{ Auth::user()->profil->foto }}" class="img-circle elevation-2" alt="User Image">
        </div>
        @endauth
        <div class="info">
          @guest
            <a href="#" class="d-block">Unregistered User</a>
          @endguest
          @auth
            <a href="/profil" class="d-block">{{ Auth::user()->name }}<br><small>{{ Auth::user()->profil->umur }} Tahun</small></a>
          @endauth
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item">
              <a href="/" class="nav-link  {{(request()->segment(1) == '') ? 'active' : ''}}">
                <i class="nav-icon fas fa-home"></i>
                <p>
                    Home
                </p>
              </a>
            </li>
            <li class="nav-item {{ (request()->segment(1) == 'profil' || request()->segment(1) == 'kategori' || request()->segment(1) == 'penulis' || request()->segment(1) == 'buku' || request()->segment(1) == 'tanggapan') ? 'menu-open' : '' }}">
              <a href="#" class="nav-link {{ (request()->segment(1) == 'profil' || request()->segment(1) == 'kategori' || request()->segment(1) == 'penulis' || request()->segment(1) == 'buku' || request()->segment(1) == 'tanggapan') ? 'active' : '' }}">
                <i class="nav-icon fas fa-cogs"></i>
                <p>Pengaturan <i class="right fas fa-angle-left"></i></p>
              </a>
                <ul class="nav nav-treeview">
                    @auth
                    <li class="nav-item">
                      <a href="/profil" class="nav-link {{(request()->segment(1) == 'profil') ? 'active' : ''}}">
                      <i class="fas fa-user-circle nav-icon"></i>
                      <p>Profil</p>
                      </a>
                    </li>
                    @endauth
                    <li class="nav-item">
                        <a href="/kategori" class="nav-link {{(request()->segment(1) == 'kategori') ? 'active' : ''}}">
                        <i class="fas fa-bookmark nav-icon"></i>
                        <p>Kategori</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/penulis" class="nav-link {{(request()->segment(1) == 'penulis') ? 'active' : ''}}">
                        <i class="fas fa-pen nav-icon"></i>
                        <p>Penulis</p>
                        </a>
                    </li>
                    @auth
                    <li class="nav-item">
                      <a href="/buku" class="nav-link {{(request()->segment(1) == 'buku') ? 'active' : ''}}">
                      <i class="fas fa-book nav-icon"></i>
                      <p>Buku</p>
                      </a>
                    </li>
                    @endauth
                </ul>
            </li>
            @auth
            <li class="nav-item mt-5">
              <a href="{{ route('logout') }}" class="nav-link btn-danger" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt"></i>
                <p>&nbsp;Logout</p>
              </a>
              <form action="{{ route('logout') }}" id="logout-form" method="POST" style="display: none;">
                @csrf
              </form>
            </li>
            @endauth
            @guest
            <li class="nav-item mt-5"> 
              <a href="/login" class="nav-link btn-success">
                <i class="nav-icon fas fa-sign-in-alt"></i>
                <p>Login</p>
              </a>
            </li>
            @endguest
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>