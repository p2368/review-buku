@extends('layout.master')

@section('title-1', 'Halaman Kategori')
@section('title-2', 'Tambah Kategori')

@section('content')
<div class="col-sm-8 mx-auto">
    <div class="d-flex">
        <a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-3"><i class="fa fa-reply"></i> Kembali</a>
    </div>
    <form action="/kategori" method="POST">
        @csrf
        <div class="form-group">
          <label for="nama">Nama Kategori</label>
          <input type="text" class="form-control" name="nama" id="nama">
          @error('nama')
              <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
          @enderror
        </div>
        <div class="d-flex justify-content-between">
            <button type="submit" class="btn btn-primary">Submit</button>
            <input type="reset" value="Reset" class="btn btn-secondary">
        </div>
    </form>
</div>
@endsection