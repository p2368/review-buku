@extends('layout.master')

@section('title-1', 'Halaman Kategori')
@section('title-2', 'Daftar Kategori')

@push('css')
    <link rel="stylesheet" href="{{asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}"/>
@endpush

@section('content')
<div class="d-flex justify-content-between">
  <a href="{{ url()->previous() }}" class="btn btn-outline-secondary mb-4"><i class="fa fa-reply"></i> Kembali</a>
  @auth
  <a href="/kategori/create"class="btn btn-primary mb-4">Tambah Kategori</a>
  @endauth
</div>
<table id="kategori" class="table table-bordered table-striped">
  <thead>
    <tr>
      <th class="text-center">#</th>
      <th class="text-center">Nama</th>
      <th class="text-center">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($kategoris as $key => $kategori)
    <tr>
      <td class="text-center">{{ $key+1 }}</td>
      <td>{{ $kategori->nama }}</td>
      <td class="text-center text-nowrap">
          <a href="/kategori/{{ $kategori->id }}" class="btn btn-info btn-sm btn" data-toggle="tooltip" data-placement="top" title="Detail {{ $kategori->nama }}" style="min-width: 34px;">
              <i class="fa fa-info"></i>
          </a>
          @auth
          <a href="/kategori/{{ $kategori->id }}/edit" class="btn btn-warning btn-sm text-white" data-toggle="tooltip" data-placement="top" title="Edit {{ $kategori->nama }}" style="min-width: 34px;">
            <i class="fa fa-edit"></i>
          </a>
          <form action="/kategori/{{ $kategori->id }}" method="POST" class="d-inline-block">
              @method('delete')
              @csrf
              <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Hapus {{ $kategori->nama }}" style="min-width: 34px;" onclick="return confirm('Anda yakin menghapus {{ $kategori->nama }}  ?')">
                  <i class="fa fa-trash"></i>
              </button>
          </form>
          @endauth
      </td>
    </tr>
    @empty
    <tr>
      <td colspan="3" class="text-center">DATA MASIH KOSONG</td>
    </tr>
    @endforelse
    </tbody>
  <tfoot>
    <tr>
      <th class="text-center">#</th>
      <th class="text-center">Nama</th>
      <th class="text-center">Action</th>
    </tr>
  </tfoot>
  </table>
@endsection

@push('script')
    <script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#kategori").DataTable();
    });
    </script>
@endpush