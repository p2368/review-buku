@extends('layout.master')

@section('judul')
    Tambah Data User
@endsection

@section('content')
<form>
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="nama" class="form-control" id="nama" placeholder="Masukkan Nama Anda">
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input type="username" class="form-control" id="username" placeholder="Masukkan Username Anda">
            </div>
            <div class="form-group">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" placeholder="Masukkan Email Anda">
                </div>
            </div>
            <div class="form-group">
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="Masukkan Password Anda">
                </div>
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection