@extends('layout.master')

@section('judul')
    Tambah Data Profil
@endsection

@section('content')
<form>
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="nama" class="form-control" id="nama" placeholder="Masukkan Nama Anda">
            </div>
            <div class="form-group">
                <label for="username">Bio</label>
                <textarea name="bio" class="form-control" cols="30" rows="10" placeholder="Masukkan Bio Anda"></textarea>
            </div>
            <div class="form-group">
                <div class="form-group">
                    <label for="email">Users_ID</label>
                    <input type="users_id" class="form-control" id="Users_ID" placeholder="Masukkan Users ID Anda">
                </div>
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
@endsection