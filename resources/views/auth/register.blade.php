<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('admin/dist/img/AdminLTELogo.png')}}" type="image/x-icon">
    <title>Halaman Registrasi | Review Buku</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('admin/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('admin/dist/css/adminlte.min.css')}}">
</head>

<body class="hold-transition register-page">
    <div class="register-box">
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <a href="/" class="h1"><b>Review</b>Buku</a>
            </div>
            <div class="card-body">
                <p class="login-box-msg">Registrasi pengguna baru</p>
                <form action="{{ route('register') }}" method="POST">
                    @csrf
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Nama Lengkap" name="name" id="name">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    @error('name')
                    <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
                    @enderror

                    <div class="input-group mb-3">
                        <input type="email" class="form-control" placeholder="Email" name="email" id="email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    @error('email')
                    <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
                    @enderror

                    <div class="input-group mb-3">
                        <input type="password" class="form-control" placeholder="Password" name="password" id="password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    @error('password')
                    <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
                    @enderror

                    <div class="input-group mb-3">
                        <input type="password" class="form-control" placeholder="Konfirmasi password" name="password_confirmation" id="password_confirmation">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    @error('password_confirmation')
                    <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
                    @enderror

                    <div class="input-group mb-3">
                        <input type="text" name="umur" id="umur" class="form-control" placeholder="Umur" pattern="[1-9][0-9]*">
                    </div>
                    @error('umur')
                    <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
                    @enderror

                    <div class="input-group mb-3">
                        <textarea name="bio" id="bio" class="form-control" placeholder="Biodata"></textarea>
                    </div>
                    @error('bio')
                    <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
                    @enderror

                    <div class="input-group mb-3">
                        <textarea name="alamat" id="alamat" class="form-control" placeholder="Alamat Lengkap Anda"></textarea>
                    </div>
                    @error('alamat')
                    <div class="alert alert-danger mt-1" role="alert">{{ $message }}</div>
                    @enderror
                    
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary btn-block mb-2">Daftar</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <a href="/login" class="text-center d-block block-center">Pernah daftar? Klik disini</a>
            </div>
            <!-- /.form-box -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.register-box -->

    <!-- jQuery -->
    <script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('admin/dist/js/adminlte.min.js')}}"></script>
</body>

</html>