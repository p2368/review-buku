<h1>Final Project</h1>

## Kelompok 5

## Anggota Kelompok

- Mohamad Fa'ul Janaqi
- Themy Sabri Syuhada
- Akbar Nur Hidayanto

## Tema Project

Rating Buku.
User yang belum mendaftar di sistem, hanya bisa melihat daftar buku, kategori, dan penulisnya. Jika ingin memberikan ulasan dan rating harus daftar dan login. User hanya dapat membuat satu ulasan dan rating per buku, dan hanya bisa mengedit dan menghapus ulasan/rating miliknya sendiri. User juga dapat mengubah halaman profil, foto, nama, dll. Dan tidak bisa mengubah profil user lain. User juga dapat melihat buku apa saja di dalam suatu kategori dan melihat buku apa saja yang sudah ditulis oleh seorang penulis.

Library yang digunakan :
- OwlCarousel
- ChartJS
- TinyMCE
- Select2
- SweetAlert
- DataTables

## ERD

<p align="center"><a href="https://cowayjkt.id/review-buku" target="_blank"><img src="https://cowayjkt.id/assets/review-buku.png" width="400"></a></p>

## Link Video

- Link Demo Aplikasi: https://www.youtube.com/watch?v=ohZ9EEidhZ8
- Link Deploy : https://cowayjkt.id/review-buku