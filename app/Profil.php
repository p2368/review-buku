<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table = "profil";
    protected $fillable = ['umur', 'bio', 'alamat', 'user_id', 'foto'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
