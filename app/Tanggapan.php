<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tanggapan extends Model
{
    protected $table = "tanggapan";
    protected $fillable = ['isi', 'rating', 'buku_id', 'user_id'];

    public function buku()
    {
        return $this->belongsTo('App\Buku');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
