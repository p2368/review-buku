<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = "buku";
    protected $fillable = ['judul', 'penerbit', 'tahun_terbit', 'jumlah_halaman', 'sinopsis', 'coverimg', 'kategori_id', 'penulis_id'];

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }

    public function penulis()
    {
        return $this->belongsTo('App\Penulis');
    }

    public function tanggapan()
    {
        return $this->hasMany('App\Tanggapan');
    }
}
