<?php

namespace App\Http\Controllers;

use App\Penulis;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class PenulisController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penulises = Penulis::all();
        return view('penulis.index', compact('penulises'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('penulis.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'bio' => 'required'
        ]);

        $penulis = new Penulis;
        $penulis->nama = $request->nama;
        $penulis->bio = $request->bio;
        $result = $penulis->save();

        if ($result) {
            Alert::success('Success', $penulis->nama . ' berhasil ditambahkan.');
            return redirect('/penulis');
        } else {
            Alert::error('Error', $penulis->nama . ' gagal ditambahkan.');
            return redirect('/penulis');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penulis = Penulis::findOrFail($id);
        return view('penulis.show', compact('penulis'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penulis = Penulis::findOrFail($id);
        return view('penulis.edit', compact('penulis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'bio' => 'required'
        ]);

        $penulis = Penulis::findOrFail($id);
        $namaTmp = $penulis->nama;
        $penulis->nama = $request->nama;
        $penulis->bio = $request->bio;
        $result = $penulis->save();

        if ($result) {
            Alert::success('Success', $namaTmp . ' berhasil diperbaharui.');
            return redirect('/penulis');
        } else {
            Alert::error('Error', $namaTmp . ' gagal diperbaharui.');
            return redirect('/penulis');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $penulis = Penulis::findOrFail($id);
        $namaTmp = $penulis->nama;
        $result = $penulis->delete();
        if ($result == true) {
            Alert::success('Success', $namaTmp . ' berhasil dihapus.');
            return redirect('/penulis');
        } else {
            Alert::error('Error', $namaTmp . ' gagal dihapus.');
            return redirect('/penulis');
        }
    }
}
