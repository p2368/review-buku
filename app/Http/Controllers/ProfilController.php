<?php

namespace App\Http\Controllers;

use App\Profil;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;

class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profil = Profil::where('user_id', Auth::id())->first();
        return view('halaman.profil', compact('profil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'umur' => 'required',
            'bio' => 'required',
            'alamat' => 'required',
            'foto' => 'image|mimes:jpeg,jpg,png|max:1024'
        ]);

        $profil = Profil::findOrFail($id);
        $profil->umur = $request->umur;
        $profil->bio = $request->bio;
        $profil->alamat = $request->alamat;
        $fotoTmp = $profil->foto;

        if ($request->has('foto')) {
            $namaFoto = time() . '.' . $request->foto->extension();
            $request->foto->move(public_path('img/profil'), $namaFoto);
            $profil->foto = $namaFoto;

            if ($fotoTmp != 'avatar.png') {
                File::delete('img/profil/' . $fotoTmp);
            }
        }

        $result = $profil->update();

        $user = User::findOrFail($profil->user_id);
        $user->name = $request->name;
        $results = $user->update();

        if ($result == true && $results == true) {
            Alert::success('Success', 'Profil ' . Auth::user()->name . ' berhasil diperbaharui.');
            return redirect('/profil');
        } else {
            Alert::error('Error', 'Profil ' . Auth::user()->name . ' gagal diperbaharui.');
            return redirect('/profil');
        }
    }
}
