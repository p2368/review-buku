<?php

namespace App\Http\Controllers;

use App\Tanggapan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class TanggapanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'rating' => 'required',
            'isi' => 'required',
        ]);

        $tanggapan = new Tanggapan;
        $tanggapan->user_id = Auth::id();
        $tanggapan->buku_id = $request->buku_id;
        $tanggapan->rating = $request->rating;
        $tanggapan->isi = $request->isi;
        $result = $tanggapan->save();

        if ($result) {
            Alert::success('Success', 'Ulasan berhasil ditambahkan.');
            return redirect('/buku/' . $tanggapan->buku_id . '#ulasan');
        } else {
            Alert::error('Error', 'Ulasan gagal ditambahkan.');
            return redirect('/buku/' . $tanggapan->buku_id . '#ulasan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tanggapan = Tanggapan::findOrFail($id);
        return view('tanggapan.edit', compact('tanggapan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'rating' => 'required',
            'isi' => 'required',
        ]);

        $tanggapan = Tanggapan::findOrFail($id);
        $tanggapan->user_id = Auth::id();
        $tanggapan->buku_id = $request->buku_id;
        $tanggapan->rating = $request->rating;
        $tanggapan->isi = $request->isi;
        $result = $tanggapan->update();

        if ($result) {
            Alert::success('Success', 'Ulasan berhasil diubah.');
            return redirect('/buku/' . $tanggapan->buku_id . '#ulasan');
        } else {
            Alert::error('Error', 'Ulasan gagal diubah.');
            return redirect('/buku/' . $tanggapan->buku_id . '#ulasan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tanggapan = Tanggapan::findOrFail($id);
        $result = $tanggapan->delete();

        if ($result == true) {
            Alert::success('Success', 'Ulasan telah dihapus.');
            return redirect('/buku/' . $tanggapan->buku_id . '#ulasan');
        } else {
            Alert::error('Error', 'Ulasan gagal dihapus.');
            return redirect('/buku/' . $tanggapan->buku_id . '#ulasan');
        }
    }
}
