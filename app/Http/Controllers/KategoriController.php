<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class KategoriController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategoris = Kategori::all();
        return view('kategori.index', compact('kategoris'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $kategori = new Kategori;
        $kategori->nama = $request->nama;
        $kategori->save();

        if ($kategori) {
            Alert::success('Success', $kategori->nama . ' berhasil ditambahkan.');
            return redirect('/kategori');
        } else {
            Alert::error('Error', $kategori->nama . ' gagal ditambahkan.');
            return redirect('/kategori');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategori = Kategori::findOrFail($id);
        return view('kategori.show', compact('kategori'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::findOrFail($id);
        return view('kategori.edit', compact('kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $kategori = Kategori::findOrFail($id);
        $namaTmp = $kategori->nama;
        $kategori->nama = $request->nama;
        $result = $kategori->save();

        if ($result) {
            Alert::success('Success', $namaTmp . ' berhasil diubah menjadi ' . $kategori->nama . '.');
            return redirect('/kategori');
        } else {
            Alert::error('Error', $namaTmp . ' gagal diubah.');
            return redirect('/kategori');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = Kategori::findOrFail($id);
        $namaTmp = $kategori->nama;
        $result = $kategori->delete();
        if ($result == true) {
            Alert::success('Success', $namaTmp . ' berhasil dihapus.');
            return redirect('/kategori');
        } else {
            Alert::error('Error', $namaTmp . ' gagal dihapus.');
            return redirect('/kategori');
        }
    }
}
