<?php

namespace App\Http\Controllers;

use App\Buku;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $bukux = Buku::orderByDesc('created_at')->limit(7)->get();
        $bukus = Buku::all();
        $charts = Buku::withCount('tanggapan')->orderBy('tanggapan_count', 'desc')->limit(5)->get();
        $user_id = Buku::with('tanggapan')->get();

        foreach ($charts as $chart) {
            $labels[] = $chart->judul;
            $datas[] = $chart->tanggapan_count;
        }

        return view('halaman.index', [
            'bukux' => $bukux,
            'bukus' => $bukus,
            'labels' => json_encode($labels, JSON_NUMERIC_CHECK),
            'datas' => json_encode($datas, JSON_NUMERIC_CHECK)
        ]);
    }
}
