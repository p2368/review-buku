<?php

namespace App\Http\Controllers;

use App\Buku;
use App\Kategori;
use App\Penulis;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class BukuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bukus = Buku::all();
        return view('buku.index', compact('bukus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategoris = Kategori::orderBy('nama')->get();
        $penulises = Penulis::orderBy('nama')->get();
        return view('buku.create', ['kategoris' => $kategoris, 'penulises' => $penulises]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->flash();
        $request->validate([
            'judul' => 'required',
            'penulis_id' => 'required_without:penulis_baru',
            'penulis_baru' => 'unique:App\Penulis,nama|required_without:penulis_id',
            'penerbit' => 'required',
            'tahun_terbit' => 'required',
            'jumlah_halaman' => 'required',
            'sinopsis' => 'required',
            'kategori_id' => 'required_without:kategori_baru',
            'kategori_baru' => 'unique:App\Kategori,nama|required_without:kategori_id',
            'coverimg' => 'required|image|mimes:jpeg,jpg,png|max:2048'
        ]);

        $namaCover = time() . '.' . $request->coverimg->extension();
        $request->coverimg->move(public_path('img/cover'), $namaCover);

        $buku = new Buku;
        $buku->judul = $request->judul;
        $buku->penulis_id = $request->penulis_id;
        $buku->penerbit = $request->penerbit;
        $buku->tahun_terbit = $request->tahun_terbit;
        $buku->jumlah_halaman = $request->jumlah_halaman;
        $buku->sinopsis = $request->sinopsis;
        $buku->kategori_id = $request->kategori_id;
        $buku->coverimg = $namaCover;

        if ($request->filled('penulis_baru')) {
            $penulis = new Penulis;
            $penulis->nama = $request->penulis_baru;
            $penulis->save();
            $buku->penulis_id = $penulis->id;
        }

        if ($request->filled('kategori_baru')) {
            $kategori = new Kategori;
            $kategori->nama = $request->kategori_baru;
            $kategori->save();
            $buku->kategori_id = $kategori->id;
        }

        $result = $buku->save();

        if ($result) {
            Alert::success('Success', $buku->judul . ' berhasil ditambahkan.');
            return redirect('/buku');
        } else {
            Alert::error('Error', $buku->judul . ' gagal ditambahkan.');
            return redirect('/buku');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::findOrFail($id);
        return view('buku.show', compact('buku'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategoris = Kategori::orderBy('nama')->get();
        $penulises = Penulis::orderBy('nama')->get();
        $buku = Buku::findOrFail($id);
        return view('buku.edit', ['kategoris' => $kategoris, 'penulises' => $penulises, 'buku' => $buku]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'penulis_id' => 'required_without:penulis_baru',
            'penulis_baru' => 'unique:App\Penulis,nama|required_without:penulis_id',
            'penerbit' => 'required',
            'tahun_terbit' => 'required',
            'jumlah_halaman' => 'required',
            'sinopsis' => 'required',
            'kategori_id' => 'required_without:kategori_baru',
            'kategori_baru' => 'unique:App\Kategori,nama|required_without:kategori_id',
            'coverimg' => 'image|mimes:jpeg,jpg,png|max:2048'
        ]);

        $buku = Buku::findOrFail($id);
        $judulTmp = $buku->judul;
        $buku->judul = $request->judul;
        $buku->penulis_id = $request->penulis_id;
        $buku->penerbit = $request->penerbit;
        $buku->tahun_terbit = $request->tahun_terbit;
        $buku->jumlah_halaman = $request->jumlah_halaman;
        $buku->sinopsis = $request->sinopsis;
        $buku->kategori_id = $request->kategori_id;
        $coverTmp = $buku->coverimg;

        if ($request->filled('penulis_baru')) {
            $penulis = new Penulis;
            $penulis->nama = $request->penulis_baru;
            $penulis->save();
            $buku->penulis_id = $penulis->id;
        }

        if ($request->filled('kategori_baru')) {
            $kategori = new Kategori;
            $kategori->nama = $request->kategori_baru;
            $kategori->save();
            $buku->kategori_id = $kategori->id;
        }

        if ($request->has('coverimg')) {
            $namaCover = time() . '.' . $request->coverimg->extension();
            $request->coverimg->move(public_path('img/cover'), $namaCover);
            $buku->coverimg = $namaCover;
            File::delete('img/cover/' . $coverTmp);
        }

        $result = $buku->update();

        if ($result) {
            Alert::success('Success', $judulTmp . ' berhasil diperbaharui.');
            return redirect('/buku');
        } else {
            Alert::error('Error', $judulTmp . ' gagal diperbaharui.');
            return redirect('/buku');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $path = "img/cover/";
        $buku = Buku::findOrFail($id);
        $judulTmp = $buku->judul;
        $result = $buku->delete();
        $resultFile = File::delete($path . $buku->coverimg);

        if ($result == true && $resultFile == true) {
            Alert::success('Success', $judulTmp . ' berhasil dihapus.');
            return redirect('/buku');
        } else {
            Alert::error('Error', $judulTmp . ' gagal dihapus.');
            return redirect('/buku');
        }
    }
}
